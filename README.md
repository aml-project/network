# Recognizing the American Sign Language Alphabet

## Advanced Machine Learning - Mini Research Project
This repository contains the code for reproducing our results.
For more details on them please refer to our report.

## Training & Test Data
* The original training data is available at [kaggle](https://www.kaggle.com/grassknoted/asl-alphabet)
* ImageNet URLs are available at the ImageNet [homepage](http://image-net.org/imagenet_data/urls/imagenet_fall11_urls.tgz).
* Our augmentation templates are available [here](https://cloud.justacid.com/s/cKs4zrR7desBLkY).
* Our custom test data set is available [here](https://cloud.justacid.com/s/HCFwB7EdWDSmESk).
* A checkpoint of our best model is available [here](https://cloud.justacid.com/s/b8anwTZekzobFKd).

For copyright reasons you have to download the ImageNet images 
yourself - there is a helper script available in the tools folder.

```bash
pipenv shell
cd tools
python dl_imgnet.py --urls imagenet_urls.txt --num-images 2000
```
This will download a random subset of 2000 images from the textfile 
*imagenet_urls.txt* and save them into the subfolder *download*.

## Setup
We recommend using [pipenv](https://github.com/pypa/pipenv) for managing
dependencies. Clone the repository and type:

```bash
git clone https://gitlab.com/aml-project/network.git
cd network
pipenv install
```

Now activate the subshell and you are all set up:
```bash
pipenv shell
python webcam.py [...]
```

## Real-Time Classification 
If available, you can use a installed webcam to play around with our best model:
```bash
python webcam.py --load-checkpoint resnet18-best.pt --model resnet18 
```

Otherwise you can watch a demo video [here](https://cloud.justacid.com/s/eMmLfGNH6ygftq9).

## Training a New Model
You can train a new model with the included *train* script, e.g.

```bash 
python train.py --folder data/asl-alphabet --backgrounds
data/imgnet-bgs --augments data/augmentation-templates --workers 8 --model
resnet18 --replace-prob 0.3 
```

Trains a 18-layer ResNet with 30% augmented samples and uses 8 worker threads
to load the data. For the training data, as well as the augmentation
templates, it is assumed that the images are sorted into subfolders according
to their labels. To reproduce our results you will have to remove the labels
*"del"*, *"space"*, *"J"* and *"Z"*.

To train a particular model type pass one of the following to the model parameter.
The default is resnet18.

| Model Type         |
| ------------------ |
| aslconvnet         |
| resnet18           |
| resnet18-freeze08  |
| resnet18-finetuned |
| resnet50           |
| resnet50-finetuned |


You can configure additional parameters on the command line, please refer to the help
for more details:

```bash
python train.py --help
```

## Testing a Trained Model
A trained model can be evaluated in several ways. First it can be evaluated on the
default train-test-split.

```bash
python test.py --load-checkpoint resnet18-best.pt --model resnet18 --folder
data/asl-alphabet
```

If you want to run tests on a whole folder (instead of the default split),
you need to pass the additional parameter
*"--test-folder"*.

```bash
python test.py --load-checkpoint resnet18-best.pt --model resnet18 --folder
data/custom-test-data --test-folder
```

Additionally you can pass the parameter *"--conf-mat"* to print a partly 
LaTeX formatted confusion matrix to the terminal. If you pass *"--plots"* instead
of *"--test-folder"*, the test script will instead show nine random plots with
predictions and their true labels from the given test folder.

As always refer to the help for more details.
```bash
python test.py --help
```
