import argparse
import datetime
import os
import pathlib

import torch
from torch.utils.data import DataLoader

from aslnet.augmentation import ImageFolder
from aslnet import logger, models, util


def main(args):
    use_gpu = torch.cuda.is_available() and not args.no_cuda
    device = torch.device("cuda" if use_gpu else "cpu")

    _, standard_transforms = models.get_model_pipeline(
        model=args.model, mode="test", device=device)
    net, augmented_transforms = models.get_model_pipeline(
        model=args.model, mode="train", pretrained=True, args=args, device=device)

    dataset = ImageFolder(root=args.folder, transform=standard_transforms)
    augmented_dataset = ImageFolder(root=args.folder, transform=augmented_transforms)
    optimizer = torch.optim.Adam(net.parameters(), lr=args.learning_rate)

    epoch_start = 0
    args.global_step = 0

    timestamp = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    if args.load_checkpoint and not args.no_checkpoints:
        epoch_start, timestamp, args.global_step = util.load_checkpoint(
            args.load_checkpoint, net, optimizer)
        epoch_start += 1

    args.train_loss_log = logger.VisdomPlotLogger(
        "line", env=timestamp, opts={"title": "Train Loss"})
    args.save_dir = pathlib.Path(args.checkpoint_dir) / timestamp

    if not args.no_checkpoints:
        os.makedirs(args.save_dir, exist_ok=True)

    num_samples = len(dataset)
    train_sampler, test_sampler, val_sampler = util.make_samplers(num_samples, random_seed=0)
    train_loader = DataLoader(
        augmented_dataset, args.batch_size, sampler=train_sampler, num_workers=args.workers)
    val_loader = DataLoader(
        dataset, args.batch_size, sampler=val_sampler, num_workers=args.workers)
    test_loader = DataLoader(
        dataset, args.batch_size, sampler=test_sampler, num_workers=args.workers)

    for epoch in range(epoch_start, args.epochs):
        models.train(net, train_loader, optimizer, args=args, epoch=epoch, device=device)

        if epoch % args.checkpoint_interval == 0 and not args.no_checkpoints:
            save_dir = pathlib.Path(args.checkpoint_dir) / timestamp
            util.save_checkpoint(
                save_dir, net, optimizer, epoch, timestamp,
                global_step=args.global_step, keep_n=args.keep_n, tag=args.tag)

        models.test(net, val_loader, epoch=epoch, mode="validate", device=device)

    models.test(net, test_loader, device=device)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="ASL Network", formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # network specific settings
    parser.add_argument(
        "-e", "--epochs", default=10, type=int,
        help="Number of epochs to train.")
    parser.add_argument(
        "-b", "--batch-size", default=64, type=int,
        help="The size of a batch for training.")
    parser.add_argument(
        "-lr", "--learning-rate", default=1e-3, type=float,
        help="The learning rate.")
    parser.add_argument(
        "-m", "--model", default="resnet18", type=str,
        help="The type of model to learn.")

    # general settings
    parser.add_argument(
        "--no-cuda", action="store_true",
        help="Cuda is enabled by default unless explicitly disabled by this flag.")
    parser.add_argument(
        "--plots", action="store_true",
        help="Create plots during training.")

    parser.add_argument(
        "-w", "--workers", default=4, type=int,
        help="Number of worker threads for data loading from disk.")
    parser.add_argument(
        "-f", "--folder", default="data/alphabet", type=str,
        help="The root folder for the ASL dataset.")
    parser.add_argument(
        "--log-interval", default=10, type=int,
        help="Show log information every N batches.")
    parser.add_argument(
        "--render-samples", action="store_true",
        help="Shows the first image of each batch in a preview window.")

    # checkpointing
    parser.add_argument(
        "--no-checkpoints", action="store_true",
        help="Whether to save training checkpoints.")
    parser.add_argument(
        "--checkpoint-dir", default="chkpts", type=str,
        help="Root dir for model checkpoints.")
    parser.add_argument(
        "--checkpoint-interval", default=1, type=int,
        help="Checkpoint after N epochs.")
    parser.add_argument(
        "--load-checkpoint", default=None, type=str,
        help="Load a given checkpoint.")
    parser.add_argument(
        "--keep-n", default=5, type=int,
        help="Keep the last n checkpoints.")
    parser.add_argument(
        "--tag", default="model", type=str,
        help="Name of the model checkpoints.")

    # image augmentation
    parser.add_argument(
        "--replace-prob", default=0.3, type=float,
        help="Augment images with given probability.")
    parser.add_argument(
        "--backgrounds", type=str, default="data/backgrounds",
        help="Directory with images used as background.")
    parser.add_argument(
        "--augments", type=str, default="data/augments",
        help="Directory with augmentation masks.")

    main(parser.parse_args())
