import argparse
import datetime
import os

import cv2
import numpy as np
from PIL import Image
import torch

from aslnet import models
from aslnet import util


def crop_to_square(image, size):
    i = int(round((image.shape[0] - size) / 2.))
    j = int(round((image.shape[1] - size) / 2.))
    return image[i:i+size, j:j+size, :]


def frame_to_tensor(image, *, transforms=None):
    frame = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    if transforms:
        frame = transforms(Image.fromarray(frame))

    # reshape to a batch_size of 1
    frame = np.reshape(np.array(frame), [1, *frame.shape])
    return torch.from_numpy(frame.astype(np.float32))


def predict_sign(model, image):
    with torch.no_grad():
        return model(image).max(1, keepdim=True)[1].item()


def draw_prediction(image, label):
    frame = image[:]
    font = cv2.FONT_HERSHEY_SIMPLEX
    font_color = (0, 255, 255)
    text = "Prediction: {0}".format(label)
    cv2.putText(frame, text, (10, 25), font, 0.7, font_color, 2)
    return frame


def save_image(image):
    os.makedirs("saved_images", exist_ok=True)
    timestamp = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    cv2.imwrite("saved_images/asl-{0}.jpg".format(timestamp), image)


def run_webcam(model, labels, *, transforms=None):
    model.eval()

    capture = cv2.VideoCapture(0)
    frozen = False
    frozen_frame = None

    while True:
        _, frame = capture.read()

        sqsize = np.min(frame.shape[:2])
        frame = crop_to_square(frame, sqsize)

        key = cv2.waitKey(1)
        if key == 27: # escape 
            return
        elif key == 32: # space
            frozen = not frozen
        elif key == ord("s") and frozen:
            save_image(frozen_frame)
        
        if not frozen:
            frozen_frame = frame.copy()
            tensor = frame_to_tensor(frame, transforms=transforms)
            prediction = predict_sign(model, tensor)
            frame = cv2.flip(frame, 1)
            image = draw_prediction(frame, labels[prediction])
            cv2.imshow("frame", image)


def main(args):
    net, transforms = models.get_model_pipeline(model=args.model, mode="test")
    util.load_checkpoint(args.load_checkpoint, net, device=torch.device("cpu"))
    run_webcam(net, models.labels, transforms=transforms)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="ASL Network")
    parser.add_argument("--load-checkpoint", type=str, help="Load a given checkpoint.")
    parser.add_argument(
        "--model", default="resnet18", type=str, help="Load a given checkpoint.")
    main(parser.parse_args())
