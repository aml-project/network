import os
import imageio
import argparse


def main(args):
    for imgpath in [x for x in os.listdir(args.images)]:
        imgpath = imgpath.split('.')[0]
        img = imageio.imread("{}/{}.jpg".format(args.images, imgpath))
        mask = imageio.imread("{}/{}.png".format(args.masks, imgpath))
        img[mask == 0] = 0
        os.makedirs("{}".format(args.output), exist_ok=True)
        imageio.imwrite("{}/{}.png".format(args.output, imgpath), img)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Reapply Masks and save without compression",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # network specific settings
    parser.add_argument("-m", "--masks", required=True,
                        help="Directory with masks to apply.")
    parser.add_argument("-i", "--images", required=True,
                        help="Directory with images that are masked.")
    parser.add_argument("-o", "--output", required=True,
                        help="Directory to store output images")

    main(parser.parse_args())
