import argparse
import glob
import os
import pickle
import time

import imageio
import matplotlib.pyplot as plt
import numpy as np
import skimage.color as col
from skimage import filters
from skimage import morphology as morph


def threshold(image, threshold=None):
    if threshold is None:
        threshold = filters.threshold_yen(image)
    outimg = image
    mask = image < threshold
    outimg[mask] = 0
    mask = image > threshold
    return outimg, mask


def cut_masks(images, backgrounds, show_images=False):
    cuts = []
    masks = []

    for i, (img, bg) in enumerate(zip(images, backgrounds)):
        _, mask = threshold(np.abs(img-bg))
        mask = col.rgb2gray(mask)
        mask = np.array(mask, dtype="bool")
        cleaned = morph.remove_small_objects(mask, 512)

        for i in range(4):
            cleaned = morph.binary_erosion(cleaned)
        for i in range(2):
            cleaned = morph.binary_dilation(cleaned)
        cleaned = np.array(cleaned, dtype="bool")

        masks.append(cleaned)
        orig = img.copy()
        img[~cleaned, :] = 0
        cuts.append(img)

        if show_images:
            plt.subplot(3, 3, 3 * i + 1)
            plt.imshow(orig)
            plt.subplot(3, 3, 3 * i + 2)
            plt.imshow(cleaned)
            plt.subplot(3, 3, 3 * i + 3)
            plt.imshow(img)

    if show_images:
        plt.show()
    return cuts, masks


def normalize_image(X, axis=(1, 2)):
    min_x = np.min(X, axis=axis)
    X = X.T
    for i in range(X.shape[0]):
        X[i, :, :, :] -= min_x[:, i].T
    X = X.T
    max_x = np.max(X, axis=axis)
    X = X.T
    for i in range(X.shape[0]):
        X[i, :, :, :] = X[i, :, :, :] / max_x[:, i].T
    X = X.T
    return X


def match_background(image_files=None, images=None, background_files="../network/data/nothing/*", 
                     show_images=False, batchnumber=3):
    if type(background_files) == str:
        file_list = glob.glob(background_files)
    else:
        file_list = background_files

    assert image_files is not None
    if not images:
        images = [np.array(imageio.imread(image_file))/255 for image_file in image_files]

    batchsize = len(file_list) // batchnumber
    assert len(file_list) % batchnumber == 0

    res = []
    for k, image in enumerate(images):
        print("++ Processing image {}.".format(image_files[k]))
        best = np.zeros((batchnumber, 200, 200, 3))
        for i in range(batchnumber):
            print(" + Processing batch {}.".format(i))
            chunk_paths = file_list[i * batchsize : (i + 1) * batchsize]
            chunk = np.array([(imageio.imread(path)/255) for path in chunk_paths])
            chunk = normalize_image(chunk)
            differences = chunk - normalize_image(np.reshape(image, [1, *image.shape]))[0]
            idx_min = np.argmin(np.sum(np.abs(differences), axis=(1, 2, 3)))
            best[i, :, :, :] = chunk[idx_min, :, :, :]
        idx_min = np.argmin(np.sum(best, axis=(1, 2, 3)))
        best = best[idx_min]
        res.append(best)
        if show_images:
            plt.subplot(3, 3, 3 * k + 1)
            plt.imshow(image)
            plt.subplot(3, 3, 3 * k + 2)
            plt.imshow(best)
            plt.subplot(3, 3, 3 * k + 3)
            plt.imshow(np.abs(image-best))

    if show_images:
        plt.show()

    return res


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="Subtract background from images and return and save masks and cut images.")
    parser.add_argument(
        "-f", "--files", nargs="+", required=False, 
        help="<Required> Specify input images with patterns.")
    parser.add_argument(
        "-p", "--pre", type=str, required=False, 
        help="Specify a precomputed pickle with images.")
    parser.add_argument(
        "--save_pre", nargs=1, required=False, default="/tmp/matched_bg.p", 
        help="Specify location to save precomputations")
    parser.add_argument(
        "-s", "--save_dict", nargs=1, required=False, default="/tmp/output.p", 
        help="Specify location of output dict as pickle with masks, cuts and lables.")
    parser.add_argument(
        "-v", "--verbose", action='store_true', default=False, 
        help="Specify if output is shown.")
    parser.add_argument(
        "-b", "--batches", type=int, default=4, 
        help="Specify number of batches to avoid memory overflows.")
    parser.add_argument(
        "-g", "--bg", nargs="+", default="../network/data/nothing/*",
        help="Specify location of backgrounds files")

    args = parser.parse_args()
    inputs = args.files
    precomputed_path = args.pre
    save_pre = args.save_pre
    verbose = args.verbose

    if not inputs:
        inputs = ["../network/data/A/A111.jpg"]

    image_files = []
    for input in inputs:
        image_files.extend(glob.glob(input))

    labels = {}
    names = {}
    for f in image_files:
        labels[f] = f.split("/")[-2]
        names[f] = f.split("/")[-1].split(".")[0]

    for label in labels.values():
        os.makedirs("cuts/{}/".format(label), exist_ok=True)
        os.makedirs("masks/{}/".format(label), exist_ok=True)

    print("Processing images:\n", image_files)
    images = [np.array(imageio.imread(image_file)) / 255 for image_file in image_files]

    start_time = time.time()
    if not precomputed_path:
        res = match_background(
            show_images=verbose, batchnumber=args.batches, image_files=image_files, 
            images=images, background_files=args.bg)
        pickle.dump(res, open(save_pre, "wb"))
        print("saved backgrounds to {}".format(save_pre))
    else:
        res = pickle.load(open(precomputed_path, "rb"))
        print("loaded from {}".format(precomputed_path))
    backgrounds = res

    cuts, masks = cut_masks(images, backgrounds, show_images=verbose)

    save_dict = {
        names[file]: {
            "cuts": cuts[i], 
            "masks": masks[i]
        } for i, file in enumerate(image_files)
    }
    
    pickle.dump(save_dict, open("masks_dict.p", "wb"))

    for file in image_files:
        imageio.imwrite(
            "cuts/{}/{}.jpg".format(labels[file], names[file]), 
            np.array(255 * save_dict[names[file]]["cuts"], dtype=np.uint8))
        imageio.imwrite(
            "masks/{}/{}.png".format(labels[file], names[file]), 
            np.array(255 * save_dict[names[file]]["masks"], dtype=np.uint8))

    print("saved cuts")
    print("saved masks")
