import argparse
import os
import pathlib

import imageio
import numpy as np
import requests
import requests.exceptions as rex
import tqdm


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--num-images", default=100, type=int)
    parser.add_argument("-f", "--folder", default="download", type=str)
    parser.add_argument("-e", "--errorpics", default="errorpics", type=str)
    parser.add_argument("-u", "--urls", default="imagenet_urls.txt", type=str)
    args = parser.parse_args()

    save_dir = pathlib.Path(args.folder)
    os.makedirs(save_dir, exist_ok=True)

    invalid = {}
    errpics_folder = pathlib.Path(args.errorpics)
    for err in os.listdir(errpics_folder):
        err_img = imageio.imread(errpics_folder / err)
        invalid[err_img.shape[:2]] = err_img

    urls = []
    with open(args.urls, "rb") as f:
        for i, line in enumerate(f):
            urls.append(line)

    indices = np.arange(len(urls))
    samples = np.random.choice(indices, size=args.num_images, replace=False)

    for idx in tqdm.tqdm(samples):
        try:
            text = urls[idx].decode("utf-8")
            synset, url = text.split("\t")
            url = url.replace("\n", "")
            filename = url.split("/")[-1]

            if not filename.endswith(".jpg"):
                continue

            response = requests.get(url, timeout=2)

            if response.ok:
                image = imageio.imread(response.content, ".jpg")
                if image.shape[:2] in invalid:
                    if np.array_equal(image, invalid[image.shape[:2]]):
                        continue
                imageio.imwrite(save_dir / filename, image)
        except rex.RequestException:
            pass
        except SyntaxError:
            pass
        except OSError:
            pass
