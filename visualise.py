import argparse

import torch

import aslnet.models as mod
from aslnet.visualisation.guided_backprop import GuidedBackprop
from aslnet.visualisation.misc_functions import get_params, convert_to_grayscale
from aslnet.visualisation.misc_functions import save_gradient_images
from aslnet.visualisation.misc_functions import get_positive_negative_saliency


def perform_guided_backprop(args):

    if args.input_image is None or args.input_class is None:
        raise Exception("Input image or target label missing.")

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    input_class = mod.labels.index(args.input_class)

    _, prep_img, target, file_name_to_export, pretrained_model = get_params(
        input_file=args.input_image, input_class=input_class, input_resize=224,
        model_type=args.model, model_path=args.load_checkpoint, device=device)

    gbp = GuidedBackprop(pretrained_model)
    guided_grads = gbp.generate_gradients(prep_img, target)
    save_gradient_images(guided_grads, file_name_to_export + '_Guided_BP_color')
    grayscale_guided_grads = convert_to_grayscale(guided_grads)
    save_gradient_images(grayscale_guided_grads, file_name_to_export + '_Guided_BP_gray')
    pos_sal, neg_sal = get_positive_negative_saliency(guided_grads)
    save_gradient_images(pos_sal, file_name_to_export + '_pos_sal')
    save_gradient_images(neg_sal, file_name_to_export + '_neg_sal')
    print('Guided backprop completed')


if __name__ == '__main__':

    desc = "Generating guided backpropagation visualisation of a certain image or images."
    parser = argparse.ArgumentParser(
        description=desc, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-i", "--input-image", type=str, help="Path to image.")
    parser.add_argument(
        "-c", "--input-class", type=str, help="Target class of input images.")
    parser.add_argument(
        "--load-checkpoint", type=str, help="Path to model given as a checkpoint.")
    parser.add_argument(
        "--model", type=str, default="resnet18", choices=["aslconvnet", "resnet18"],
        help="Type of model checkpoint.")

    perform_guided_backprop(parser.parse_args())
