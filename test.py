import argparse

import matplotlib.pyplot as plt
import numpy as np
import sklearn.metrics as metrics
import torch
from torch.utils.data import DataLoader
from torchvision.datasets import ImageFolder

from aslnet import models, util


def confusion_matrix(net, loader, *, device=torch.device("cpu")):
    net.eval()
    predictions = []
    targets = []

    with torch.no_grad():
        for data, target in loader:
            data, target = data.to(device), target.to(device)
            output = net(data)
            pred = output.max(1, keepdim=True)[1]
            predictions.extend(np.reshape(pred.numpy(), pred.shape[0]))
            targets.extend(target.numpy())

    return metrics.confusion_matrix(targets, predictions)


def print_confusion_matrix(matrix, labels):
    print("  & ", end="")
    for j in range(matrix.shape[1]-1):
        print(labels[j], end="")
        print(" & ", end="")
    print(labels[j+1], end="")
    print(r" \\ \midrule")
    for i in range(matrix.shape[0]):
        print(labels[i], end="")
        print(" & ", end="")
        for j in range(matrix.shape[1]-1):
            print(matrix[i][j], end="")
            print(" & ", end="")
        print(matrix[i][j+1], end="")
        print(r" \\")
    print(r"\bottomrule")


def plot_some(model, labels, loader):
    model.eval()
    num_samples = 9

    with torch.no_grad():
        data, target = next(iter(loader))
        output = model(data)
        prediction = output.max(1, keepdim=True)[1]

    images = data[:num_samples, :, :]
    predicted = prediction[:num_samples]
    targets = target[:num_samples]

    plt.figure(figsize=(10, 10))
    plt.subplots_adjust(hspace=0.3)
    for i in range(9):
        image = images[i].permute(1, 2, 0).numpy()
        pred_label = labels[predicted[i]]
        target_label = labels[targets[i]]

        assert image.dtype == np.float32
        image += np.abs(image.min())
        image *= (1.0 / image.max())

        plt.subplot(3, 3, i+1)
        plt.imshow(image)
        plt.title("Predicted: {0} - Target: {1}".format(pred_label, target_label))

    plt.show()


def main(args):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    net, transforms = models.get_model_pipeline(model=args.model, mode="test")
    util.load_checkpoint(args.load_checkpoint, net, device=device)

    dataset = ImageFolder(root=args.folder, transform=transforms)
    _, test_sampler, _ = util.make_samplers(len(dataset), random_seed=0)

    if args.plots:
        test_loader = DataLoader(
            dataset, batch_size=64, shuffle=True, num_workers=args.workers)
        plot_some(net, models.labels, test_loader)
    elif args.test_folder:
        test_loader = DataLoader(
            dataset, batch_size=64, shuffle=True, num_workers=args.workers)
        models.test(net, test_loader)
        if args.conf_mat:
            print("Confusion Matrix:")
            mat = confusion_matrix(net, test_loader)
            print_confusion_matrix(mat, models.labels)
    else:
        test_loader = DataLoader(
            dataset, batch_size=64, sampler=test_sampler, num_workers=args.workers)
        models.test(net, test_loader)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Evaluate networks.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "--load-checkpoint", type=str, help="Load a given checkpoint.")
    parser.add_argument(
        "--model", default="resnet18", type=str, help="Load a given checkpoint.")
    parser.add_argument(
        "--plots", action="store_true", help="Load a given checkpoint.")
    parser.add_argument(
        "--conf-mat", action="store_true", help="Print the confusion matrix.")
    parser.add_argument(
        "--test-folder", action="store_true", help="Run test on given folder.")
    parser.add_argument(
        "-f", "--folder", default="data/alphabet", type=str,
        help="The root folder for ASL dataset.")
    parser.add_argument(
        "-w", "--workers", default=4, type=int,
        help="Number of worker threads for data loading from disk.")
    main(parser.parse_args())
