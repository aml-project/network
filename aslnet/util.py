import numpy as np
import os
from pathlib import Path
import torch
from torch.utils.data.sampler import SubsetRandomSampler


def make_samplers(num_samples, test_size=0.3, val_size=0.1, *, random_seed=None):
    if random_seed is not None:
        np.random.seed(random_seed)
    indices = np.arange(num_samples)
    np.random.shuffle(indices)
    idx1 = int(num_samples * test_size)
    idx2 = int(num_samples * (test_size+val_size))
    test_idx, val_idx, train_idx = indices[:idx1], indices[idx1:idx2], indices[idx2:]
    train_sampler = SubsetRandomSampler(train_idx)
    test_sampler = SubsetRandomSampler(test_idx)
    val_sampler = SubsetRandomSampler(val_idx)
    return train_sampler, test_sampler, val_sampler


def save_checkpoint(directory, model, optimizer, epoch, timestamp, *,
                    global_step=0, keep_n=5, tag="model"):
    if keep_n:
        splitter = lambda x: int(x[len(tag)+1:len(x)-len(".pt")])
        filenames = sorted(os.listdir(Path(directory)), key=splitter)
        if len(filenames) >= keep_n:
            os.remove(Path(directory) / filenames[0])

    checkpoint = {
        "model_state": model.state_dict(),
        "optimizer_state": optimizer.state_dict(),
        "epoch": epoch,
        "timestamp": timestamp,
        "global_step": global_step
    }
    torch.save(checkpoint, Path(directory) / "{0}-{1}.pt".format(tag, epoch))


def load_checkpoint(path, model, optimizer=None, device=None):
    checkpoint = torch.load(Path(path), map_location=device)
    model.load_state_dict(checkpoint["model_state"])
    if optimizer:
        optimizer.load_state_dict(checkpoint["optimizer_state"])
    return checkpoint["epoch"], checkpoint["timestamp"], checkpoint["global_step"]
