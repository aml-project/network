import os
from random import randrange, choice, random, uniform

import imageio
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
from skimage.exposure import adjust_gamma
from skimage.transform import warp, AffineTransform
import torchvision.datasets as datasets
import torchvision.transforms as transforms


class ImageAugmentation:

    def __init__(self, backgrounds, cuts, replace_prob, plot=False,
                 shear_prob=0.5, rotation_prob=0.5, gamma_prob=0.5, scale_prob=0.5,
                 max_shear=0.25, max_rotation=35, max_gamma=0.5, max_scale=0.8):
        self.backgrounds = backgrounds
        self.cuts = cuts
        self.plot = plot
        self.replace_prob = replace_prob
        self.shear_prob = shear_prob
        self.rotation_prob = rotation_prob
        self.gamma_prob = gamma_prob
        self.scale_prob = scale_prob
        self.max_shear = max_shear
        self.max_rotation = max_rotation/180
        self.max_gamma = max_gamma
        self.max_scale = max_scale

    def __call__(self, img, target):
        if self.replace_prob < random():
            return img
        cutimage = self.get_random_image("{}/{}".format(self.cuts, target))

        # transformations
        shear = uniform(-self.max_shear, self.max_shear) if self.shear_prob >= random() else 0
        rotation = uniform(-self.max_rotation, self.max_rotation) if self.rotation_prob >= random() else 0
        gamma = uniform(0, self.max_gamma) if self.gamma_prob >= random() else 0
        scale = uniform(self.max_scale, 1) if self.scale_prob >= random() else 1
        tform = AffineTransform(shear=shear, rotation=rotation, scale=(scale, scale))
        warpimage = warp(cutimage, tform,
                         output_shape=cutimage.shape,
                         preserve_range=True, order=0).astype(np.uint8)
        warpimage = adjust_gamma(warpimage, 1-gamma)

        # replace background
        while True:
            bgimage = ImageAugmentation.get_random_image(self.backgrounds)
            bgimage = ImageAugmentation.get_random_crop(bgimage, cutimage.shape)
            if bgimage is not None:
                break
        resulting_image = ImageAugmentation.replace_background(warpimage, bgimage)

        # plot debug output
        if self.plot:
            plt.subplot(121)
            plt.title("Target {}".format(target))
            plt.imshow(ImageAugmentation.replace_background(cutimage, bgimage))
            plt.subplot(122)
            plt.title("Shear {:.2f}, Rotation {:.2f}\u00b0 \nGamma {:.2f}, Scale {:.2f}"
                      .format(shear, rotation*180, gamma, scale))
            plt.imshow(resulting_image)
            plt.show()

        return Image.fromarray(resulting_image)

    @staticmethod
    def replace_background(cutimage, bgimage):
        assert cutimage.shape == bgimage.shape
        resulting_image = bgimage.copy()
        resulting_image[cutimage != 0] = cutimage[cutimage != 0]
        return resulting_image

    @staticmethod
    def get_random_crop(image, shape):
        if image.shape[0] >= shape[0] and image.shape[1] >= shape[1]:
            x = 0
            y = 0
            if image.shape[0] > shape[0]:
                x = randrange(image.shape[0]-shape[0])
            if image.shape[1] > shape[1]:
                y = randrange(image.shape[1]-shape[1])
            resulting_image = image[x:x+shape[0], y:y+shape[1]]
            return resulting_image
        else:
            return None

    @staticmethod
    def get_random_image(path):
        img = choice([x for x in os.listdir(path)])
        return imageio.imread("{}/{}".format(path, img))


class ImageFolder(datasets.ImageFolder):

    def __getitem__(self, index):
        # copied from DatasetFolder and adjusted
        path, target = self.samples[index]
        sample = self.loader(path)
        if self.transform is not None:
            if isinstance(self.transform, Compose):
                sample = self.transform(sample, self.classes[target])
            else:
                sample = self.transform(sample)
        if self.target_transform is not None:
            target = self.target_transform(target)

        return sample, target


class Compose(transforms.Compose):

    def __call__(self, img, target=None):
        # copied from Compose and adjusted
        for t in self.transforms:
            if isinstance(t, ImageAugmentation):
                img = t(img, target)
            else:
                img = t(img)
        return img
