import torch
import torch.nn.functional as f
from torchvision import transforms
from torchvision.models import resnet18, resnet50, vgg16, vgg19

from .augmentation import ImageAugmentation, Compose
from .network import AslConvNet


labels = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M",
          "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y",
          "nothing"]


def get_model_pipeline(model="resnet18", device=torch.device("cpu"), *,
                       mode="train", pretrained=False, args=None):

    pipeline = []
    if mode == "train":
        pipeline += [ImageAugmentation(backgrounds=args.backgrounds, cuts=args.augments,
                                       replace_prob=args.replace_prob, plot=args.plots),
                     transforms.RandomHorizontalFlip()]

    if "resnet" in model or "vgg" in model:
        pipeline += [transforms.Resize((224, 224)),
                     transforms.ToTensor(),
                     transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                          std=[0.229, 0.224, 0.225])]
    elif model == "aslconvnet":
        pipeline += [transforms.Resize((200, 200)), transforms.ToTensor()]

    pipeline = Compose(pipeline)

    if model == "resnet18":
        net = resnet18(pretrained=pretrained)
        net.fc = torch.nn.Linear(net.fc.in_features, len(labels))
        return net.to(device), pipeline

    if model == "resnet18-freeze08":
        net = resnet18(pretrained=pretrained)
        for param in net.parameters():
            param.requires_grad = False
        for child in list(net.children())[-4:]:
            for param in child.parameters():
                param.requires_grad = True
        net.fc = torch.nn.Linear(net.fc.in_features, len(labels))
        return net.to(device), pipeline

    if model == "resnet18-finetuned":
        net = resnet18(pretrained=pretrained)
        for param in net.parameters():
            param.requires_grad = False
        net.fc = torch.nn.Linear(net.fc.in_features, len(labels))
        return net.to(device), pipeline

    if model == "resnet50":
        net = resnet50(pretrained=pretrained)
        net.fc = torch.nn.Linear(net.fc.in_features, len(labels))
        return net.to(device), pipeline

    if model == "resnet50-finetuned":
        net = resnet50(pretrained=pretrained)
        for param in net.parameters():
            param.requires_grad = False
        net.fc = torch.nn.Linear(net.fc.in_features, len(labels))
        return net.to(device), pipeline

    if model == "aslconvnet":
        net = AslConvNet(num_classes=len(labels))
        return net.to(device), pipeline
    
    raise NotImplementedError("No transform pipeline for '{0}'".format(model))


def train(net, loader, optimizer, *, args=None, epoch=0, device=torch.device("cpu")):
    net.train()

    for batch_idx, (data, target) in enumerate(loader):
        data, target = data.to(device), target.to(device)

        output = net(data)
        loss = f.cross_entropy(output, target)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if batch_idx % args.log_interval == 0:
            args.train_loss_log.log(args.global_step, loss.item())
            print("Epoch {0}: [{1}/{2} ({3:.0f}%)] -- Loss: {4:.6f}".format(
                epoch, batch_idx * len(data), len(loader.sampler),
                100. * batch_idx/len(loader), loss.item()))

        args.global_step += 1


def test(net, loader, *, epoch=None, mode=None, device=torch.device("cpu")):
    net.eval()
    loss = 0
    correct = 0

    with torch.no_grad():
        for data, target in loader:
            data, target = data.to(device), target.to(device)

            output = net(data)
            loss += f.cross_entropy(output, target)

            pred = output.max(1, keepdim=True)[1]
            correct += pred.eq(target.view_as(pred)).sum().item()

        loss /= len(loader)

    if mode == "validate":
        print("Epoch {0}: -- Validation -- Loss: {1:.6f}, Accuracy: {2:.2f}%".format(
            epoch, loss.item(), 100. * correct / len(loader.sampler)))
    else:
        print("Testing -- Loss: {0:.6f}, Accuracy: {1:.2f}%".format(
            loss.item(), 100. * correct / len(loader.sampler)))
