import torchnet.logger as tnt
import requests


class VisdomPlotLogger(tnt.VisdomPlotLogger):

    def __init__(self, *args, **kwargs):
        self.connected = False
        try:
            port = kwargs.get("port", 8097)
            if requests.get("http://localhost:{0}".format(port)):
                self.connected = True
                super().__init__(*args, **kwargs)
                print("Initialized visdom plot logger...")
        except:
            pass

    def log(self, *args, **kwargs):
        if self.connected:
            super().log(*args, **kwargs)


class VisdomTextLogger(tnt.VisdomTextLogger):

    def __init__(self, *args, **kwargs):
        self.connected = False
        try:
            port = kwargs.get("port", 8097)
            if requests.get("http://localhost:{0}".format(port)):
                self.connected = True
                super().__init__(*args, **kwargs)
                print("Initialized visdom text logger...")
        except:
            pass

    def log(self, *args, **kwargs):
        if self.connected:
            super().log(*args, **kwargs)


class VisdomLogger(tnt.VisdomLogger):

    def __init__(self, *args, **kwargs):
        self.connected = False
        try:
            port = kwargs.get("port", 8097)
            if requests.get("http://localhost:{0}".format(port)):
                self.connected = True
                super().__init__(*args, **kwargs)
                print("Initialized visdom logger...")
        except:
            pass

    def log(self, *args, **kwargs):
        if self.connected:
            super().log(*args, **kwargs)
